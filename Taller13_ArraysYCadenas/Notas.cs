﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taller13_ArraysYCadenas
{
    class Notas
    {
        private float[] notas;
        // private float[] notas;
        public float[] SetGetNotas
        {
            get { return this.notas; }
            set
            {
                for (int i = 0; i < this.notas.Length; i++)
                {
                    if (value[i] > 5f)
                        this.notas[i] = 5f;
                    else if (value[i] < 0)
                        this.notas[i] = 0f;

                    this.notas = value;
                }
            }
        }
        private float promedio;

        public Notas(int cantidadNotas)
        {
            this.notas = new float[cantidadNotas];
            Console.WriteLine(notas.Length);
        }

        public float GetPromedioNotas()
        {
            float sum = 0;
            foreach (float nota in this.notas)
            {
                sum += nota;
            }
            this.promedio = sum / (this.notas.Length);
            return this.promedio;
        }

        public void CuantoFaltaParaElFinal()
        {
            Console.WriteLine(String.Format("promedio {0:0.00}", this.promedio));
            float faltaPalFinal = (3 - this.promedio) / 0.16f;

            if (faltaPalFinal >= 0 && faltaPalFinal < 3)
            {
                Console.WriteLine("Ya te falta poco, estudia duro, te falta {0} para pasar la materia", faltaPalFinal);
            }
            else if (faltaPalFinal >= 3 && faltaPalFinal <= 5)
            {
                Console.WriteLine("Aun puedes, estudia duro, te falta {0} para pasar la materia", faltaPalFinal);
            }
            else if (faltaPalFinal > 5)
                Console.WriteLine("Ya no tienes esperanza, estudia duro en estas vacaciones", faltaPalFinal);
            else
                Console.WriteLine("Ya pasaste, puedes seguir entrenandote en lo q mas te gusta");
        }

        public void PrintArray()
        {
            Console.Write("[");
            for (int i = 0; i < notas.Length; i++)
            {
                Console.Write(notas[i]);
                if (i < notas.Length - 1)
                {
                    Console.Write(" _ ");
                }
            }
            Console.Write("]\n");
        }

        public void PrintReverseArray()
        {
            float[] reverseArray = this.notas;
            Array.Reverse(reverseArray);
            Console.Write("[");
            for (int i = 0; i < reverseArray.Length; i++)
            {
                Console.Write(reverseArray[i]);
                if (i < reverseArray.Length - 1)
                {
                    Console.Write(" _ ");
                }
            }
            Console.Write("]\n");
        }
    }
}
