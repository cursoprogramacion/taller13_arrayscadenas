﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taller13_ArraysYCadenas
{
    class Palabras
    {
        public string[] listaPalabras;
        public Palabras(string[] palabras)
        {
            this.listaPalabras = palabras;
        }

        public string GetRandomWord()
        {
            Random rnd = new Random();
            return listaPalabras[rnd.Next(listaPalabras.Length)];
        }

        private string ReverseString(string palabra)
        {
            char[] reverseWord = palabra.ToCharArray();
            Array.Reverse(reverseWord);
            return new String(reverseWord);
        }

        public void esPalabraPolindroma(string palabra)
        {
            if (palabra == this.ReverseString(palabra))
            {
                Console.WriteLine("{0} es una palabra polindroma.", palabra);
            }
            else
            {
                Console.WriteLine("{0} NO es una palabra polindroma.", palabra);
            }
        }
    }
}
