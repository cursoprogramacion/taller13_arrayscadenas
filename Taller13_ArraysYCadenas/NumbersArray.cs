﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taller13_ArraysYCadenas
{
    public class NumbersArray
    {
        private float[] numbers;

        public NumbersArray(int n)
        {
            numbers = new float[n];
            for (int i = 1; i < numbers.Length; i++)
            {
                numbers[i] = numbers[i - 1] + 1;
            }
        }

        // Manejar excepcion IndexOutOfRange
        public float GetNumberAt(int i)
        {
            return numbers[i];
        }

        public void PrintArray()
        {
            Console.Write("[");
            for (int i = 0; i < numbers.Length; i++)
            {
                Console.Write(numbers[i]);
                if (i < numbers.Length - 1)
                {
                    Console.Write(", ");
                }
            }
            Console.Write("]\n");
        }

        public void PrintReverseArray()
        {
            float[] reverseArray = this.numbers;
            Array.Reverse(reverseArray);
            Console.Write("[");
            for (int i = 0; i < reverseArray.Length; i++)
            {
                Console.Write(reverseArray[i]);
                if (i < reverseArray.Length - 1)
                {
                    Console.Write(", ");
                }
            }
            Console.Write("]\n");
        }
    }
}
