﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taller13_ArraysYCadenas
{
    public class Program
    {
        public static void Main()
        {
            // Array de numeros
            NumbersArray numbers = new NumbersArray(10);
            numbers.PrintArray();
            Console.WriteLine("El quinto elemento del array es: {0}", numbers.GetNumberAt(5));
            numbers.PrintReverseArray();

            Console.WriteLine();

            // Notas y promedios
            Notas notas = new Notas(5);
            notas.SetGetNotas = new float[5] { 1.10f, -2.20f, 4.0f, 6.0f, 4f};
            Console.WriteLine( "Llevas la materia en {0}", notas.GetPromedioNotas() );
            Console.WriteLine("Notas en orden: ");
            notas.PrintArray();
            notas.PrintReverseArray();
            notas.CuantoFaltaParaElFinal();

            Console.WriteLine();

            // Palabras
            Palabras palabras = new Palabras(new string[5] {
                "sapo",
                "avion",
                "orejero",
                "helicoptero",
                "ana"});
            palabras.esPalabraPolindroma(palabras.GetRandomWord());

            // Para que no se cierre de inmediato
            Console.ReadKey();
        }
    }
}
